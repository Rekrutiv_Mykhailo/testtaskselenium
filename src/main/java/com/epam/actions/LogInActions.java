package com.epam.actions;

import com.epam.pages.LoginPage;
import com.epam.tools.WebActions;
import org.openqa.selenium.WebDriver;

public class LogInActions {
    LoginPage loginPage;
    WebActions webActions;

    public LogInActions(WebDriver webDriver, int wait) {
        loginPage = new LoginPage(webDriver);
        webActions = new WebActions(webDriver, wait);
    }

    public void logIn(String mail, String password) {
        typeMail(mail);
        webActions.click(loginPage.getButtonMailNext());
        typePassword(password);
        webActions.click(loginPage.getButtonPasswordNext());
    }

    public void typeMail(String mail) {
        webActions.writeText(loginPage.getEmailInput(), mail);
    }

    public void typePassword(String password) {
        webActions.writeText(loginPage.getPasswordInput(), password);
    }

    public boolean assertLogIn(String mail) {
        return loginPage.getTestFieldChecker().getText().equals(mail);
    }
}
