package com.epam.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Map;

public class DriverInitializator {
    private static Map<String, WebDriver> DRIVERS = new HashMap<>();

    static {
        DRIVERS.put("chrome", null);
        DRIVERS.put("firefox", null);
    }

    public static WebDriver getDriver(String nameBrowser) {
        WebDriver webDriver;
        switch (nameBrowser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "C:\\chromedrive\\chromedriver.exe");
                if (DRIVERS.get(nameBrowser) == null) {
                    DRIVERS.replace(nameBrowser, new ChromeDriver());
                }
                webDriver = DRIVERS.get(nameBrowser);
                break;
            case "firefox":
                webDriver = new FirefoxDriver();
                break;
            default:
                webDriver = new ChromeDriver();
        }
        return webDriver;
    }
}
