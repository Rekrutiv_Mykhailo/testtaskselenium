package com.epam.tools;

import com.epam.driver.DriverInitializator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebActions {
    private WebDriver webDriver;
    private WebDriverWait wait;

    public WebActions(WebDriver webDriver, int waitTime) {
        this.webDriver = webDriver;
        this.wait = new WebDriverWait(webDriver, waitTime);
    }

    public void goToPage(WebDriver webDriver, String pageUrl) {
        webDriver.get(pageUrl);
    }

    public void waitVisibility(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public void click(WebElement webElement) {
        waitVisibility(webElement);
        webElement.click();
    }

    public void writeText(WebElement webElement, String text) {
        waitVisibility(webElement);
        webElement.sendKeys(text);
    }

    public String readText(WebElement webElement) {
        waitVisibility(webElement);
        return webElement.getText();
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public WebDriverWait getWait() {
        return wait;
    }
}
