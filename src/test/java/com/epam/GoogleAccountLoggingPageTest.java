package com.epam;

import com.epam.actions.LogInActions;
import com.epam.driver.DriverInitializator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GoogleAccountLoggingPageTest {
    public LogInActions logInActions;
    public WebDriver webDriver;

    @BeforeTest
    public void setupTest() {
        webDriver = DriverInitializator.getDriver("chrome");
        logInActions = new LogInActions(webDriver, 5);
    }

    @Test
    public void testLoggingGoogleAccount() throws InterruptedException {
        logInActions.logIn("tttest.mmail@gmail.com", "Qwerty12!@");
        Assert.assertTrue(logInActions.assertLogIn("tttest.mmail@gmail.com"), "You have logged in");
        webDriver.quit();
    }

    @AfterTest
    public void finishTest(){
        webDriver.quit();
    }


}
